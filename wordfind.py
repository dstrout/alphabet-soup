#!/usr/bin/python

# wordfind.py - Find all of the words in an NxM letter grid
# where words can be forward, backward, or on the diagonals

# Dave Strout - 4/15/19

# Approach:
# We could have flattened the grid into a big string
# and then done substring searched and some math to reconstruct
# the x-y coordinates for reporting.  That's boring and
# if I was going to do that, I'd write it in C.  Since I
# have to use python, we may as well python it
#
# Instead, we are going to do this the same way I do it in
# on paper:
#
#  For each cell of the grid (until we run out of words):
#    Do we have any words remaining that start with that
#        letter?  Call this "subset_1.
#    If no, go to next cell in grid
#    If yes, look at the surrounding 8 letters.  Are any of them
#        the second letter in one of the words in subset_1?
#        if yes, we now have a vector.  Follow the vector,
#            if every element of the vector matches the word
#             that initially established the vector, we just
#             found a word.  Print it out and remove it from
#             the master word list, then repeat.
#        if no, remove word from subset_1 and then check the
#             next word (if any) in subset 1

# Globals:
grid=[]
# Immutable-by-agreement N+2 x M+2 array of letters.
# (technically a list of lists) The grid is padded with a
# delimiter character.  Since we are looking at the
# surrounding letters and the delimiter character  will never appear in
# the search data, this provides a border without having to
# dirty the code with "if x[y] is out of bounds" checkes everywhere.

# Example:

#            +X -> 
# grid =  [['0','0','0','0','0'],
#       +  ['0','a','b','c','0'],
#       Y  ['0','d','e','f','0'],
#       |  ['0','g','h','i','0'],
#       V  ['0','0','0','0','0']]


search_list={}
# A consumable dictionary of the words for which we are looking, key'd by the first letter.
# Example:
# ('search_list: ', {'a': [['a', 'b', 'c'], ['a', 'd', 'e', 'f']],
#            'b': [['b', 'c', 'd']]})


# Constants
border_char='0'



# compass_dict lists how to move the "cursor" when looking along a specific direction for a word
compass_dict = {1:[0,-1],  # North
                2:[1,-1],  # North East
                3:[1,0],   # East
                4:[1,1],   # South East
                5:[0,1],   # South
                6:[-1,1],  # South West
                7:[-1,0],  # West
                8:[-1,-1]} # North West

############################
#
#  check_word
#
# Check for search_word starting at row,col running in direction.  Recurses along
# the same vector until we find all the letters or hit one that isn't in the search word.
###########################
def check_word(row,col,search_word,direction):
    global grid
    word=list(search_word) # duplicate as "word" will be consumed
    delta_x=compass_dict[direction][0]
    delta_y=compass_dict[direction][1]

    if(word[0] != grid[row][col] or grid[row][col] == border_char): # Character mis-match so fail
        return 0,0
    else:
       del word[0]
       if(len(word) == 0): # We found all the letters
           return row,col 
       else: # check the next letter in line
           return check_word(row+delta_y, col+delta_x,word, direction)

# end def check_word

#################
# main code
#################


# Read input file

filepath = './input.txt'
with open(filepath) as fp:
    line = fp.readline()
    rows = int(list(line)[0])
    cols = int(list(line)[2])
    cnt = 0 # count of rows
    grid.append([0] * (cols+2)) # Top row - all zeros + 2 for the left / right border
    cnt += 1

    while line and cnt <= rows:
        line = fp.readline()
        line = ''.join(line.split()) # remove whitespace

        grid.append(list(str('0')+line+str('0')))
        cnt += 1
    grid.append([0] * (cols+2)) # Bottom row - all zeros + 2 for the left / right border

# End of reading grid -  Now read search words and append to the search_list by first letter

    line = fp.readline()
    while line:
        line = ''.join(line.split()) # remove whitespace
        try:
            search_list[line[0]] # see if there is an entry in search_list for first char of line
        except:
            search_list[line[0]] = list() # If not, put an empty list there so we have something to append to
        search_list[line[0]].append(list(line))
        line = fp.readline()
fp.close()

# End file read

grid_x=len(grid[0])-1 # number of columns (X)
grid_y=len(grid)-1 # number of rows (Y)

# Start walking through each cell
row=1
while row < grid_y:
    col=1
    while col < grid_x:

        key=grid[row][col]
        wordlist={}
        try:
            wordlist=list(search_list[key]) # see if there are any words that start with the letter in the current grid location
        except:
            pass

        while len(wordlist) > 0:
            for seekword in wordlist:
                for search_direction in range(1,9): # iterate across all 8 compass directions
                    end_x, end_y = check_word(row,col,seekword,search_direction)
                    if (end_x != 0): # only need to check x, as it's either 0,0 or success
                        printable_word = ''.join(seekword) # make it readable
                        print printable_word+" "+str(row-1)+":"+str(col-1)+" "+str(end_x-1)+":"+str(end_y-1)
                        break # No point in checking the other directions for this seekword
                wordlist.remove(seekword)
        col += 1 # end walk columns
    row += 1 # end walk rows

# End
